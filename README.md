---

## Setting Up the Project

To set up the project, follow these steps:

1. Clone the repository to your local machine:
```
  git clone https://gitlab.com/KamronBegmatov/php-task.git
```
2. Create and configure .env file:
```
  cp .env.example .env
```
3. Run `docker-compose up` to build and start the containers:
```
  docker-compose up
```

---

## Using the Application

Once the Docker containers are running, you can access the application at `http://localhost:8000/index.php`.

### Inserting Data

To insert the data from `dataset.txt` into the database, ensure that the `users` table is empty and then access `index.php`. The script will automatically populate the database with the CSV data.

### Viewing Data

Access the `index.php` with the appropriate query parameters to filter and paginate the data. For example:

http://localhost:8000/index.php?page=1&records_per_page=10&category=toys


This will display the first page of the dataset filtered by the category "toys", with 10 records per page.

### Filters

The application supports filtering by the following parameters:

* `category`
* `gender`
* `birthDate`
* `age`
* `ageRange`

### Pagination

Use `page` and `records_per_page` parameters to navigate through paginated results.

---

## Contact

If you want to contact me, you can reach me at `kamronbek@begmatov.com`.

---
