<?php

include 'connection.php';

global $pdo;

try {
    // Check if data has already been inserted
    $checkDataStmt = $pdo->query("SELECT COUNT(*) FROM users");
    $dataCount = $checkDataStmt->fetchColumn();

    if ($dataCount == 0) {
        // Insert data from CSV only if the table is empty
        $filename = "dataset.txt";
        insertDataFromCSV($pdo, $filename);
    }

    // Default values for pagination
    $page = $_GET['page'] ?? 1;
    $recordsPerPage = $_GET['records_per_page'] ?? 10;

    // Retrieve filter parameters
    $filters = [
        'category' => $_GET['category'] ?? null,
        'gender' => $_GET['gender'] ?? null,
        'birthDate' => $_GET['birthDate'] ?? null,
        'age' => $_GET['age'] ?? null,
        'ageRange' => $_GET['ageRange'] ?? null,
    ];

    // Output data as JSON API
    outputDataAsJSON($pdo, $filters, $page, $recordsPerPage);

} catch (Exception $e) {
    die("Error: " . $e->getMessage());
}

function insertDataFromCSV($pdo, $filename, $chunkSize = 1000) {
    // Check if the file exists
    if (!file_exists($filename) || !is_readable($filename)) {
        die("File not found or is not readable.");
    }

    // Open the file for reading
    $handle = fopen($filename, "r");
    if ($handle === false) {
        throw new Exception("Error opening the file.");
    }

    // Read the header row
    $header = fgetcsv($handle);

    // Prepare an SQL statement for inserting data
    $sql = "INSERT INTO users (category, firstname, lastname, email, gender, birthDate) VALUES (?, ?, ?, ?, ?, ?)";
    $stmt = $pdo->prepare($sql);

    // Initialize a counter for the batch size
    $batchCount = 0;
    $batchData = [];

    // Read the CSV file line by line
    while (($row = fgetcsv($handle)) !== false) {
        // Combine the header and row to form an associative array
        $batchData[] = array_combine($header, $row);
        $batchCount++;

        // When the batch size is reached or the file ends, insert the batch
        if ($batchCount === $chunkSize || feof($handle)) {
            foreach ($batchData as $data) {
                // Execute the prepared statement with data from each row
                $stmt->execute([$data['category'], $data['firstname'], $data['lastname'], $data['email'], $data['gender'], $data['birthDate']]);
            }
            // Reset the batch
            $batchData = [];
            $batchCount = 0;
        }
    }

    // Close the file
    fclose($handle);
}

function outputDataAsJSON($pdo, $filters = [], $page = 1, $recordsPerPage = 10)
{
    // Calculate the offset
    $offset = ($page - 1) * $recordsPerPage;

    // Start the SQL query
    $query = "SELECT * FROM users WHERE 1=1";
    $params = [];
    $paramTypes = []; // Array to hold parameter types

    // Add filters to the query
    foreach ($filters as $key => $value) {
        if ($value !== null) {
            if ($key == 'ageRange') {
                list($ageStart, $ageEnd) = explode('-', $value);
                $query .= " AND TIMESTAMPDIFF(YEAR, birthDate, CURDATE()) BETWEEN ? AND ?";
                $params[] = $ageStart;
                $params[] = $ageEnd;
                $paramTypes[] = PDO::PARAM_INT;
                $paramTypes[] = PDO::PARAM_INT;
            } else if ($key == 'age') {
                $query .= " AND TIMESTAMPDIFF(YEAR, birthDate, CURDATE()) = ?";
                $params[] = $value;
                $paramTypes[] = PDO::PARAM_INT;
            } else {
                $query .= " AND $key = ?";
                $params[] = $value;
                $paramTypes[] = is_numeric($value) ? PDO::PARAM_INT : PDO::PARAM_STR;
            }
        }
    }

    // Add pagination to the query
    $query .= " LIMIT ? OFFSET ?";
    $params[] = $recordsPerPage;
    $params[] = $offset;
    $paramTypes[] = PDO::PARAM_INT;
    $paramTypes[] = PDO::PARAM_INT;

    // Prepare the query
    $stmt = $pdo->prepare($query);

    // Bind the parameters with the respective types
    foreach ($params as $key => $value) {
        $stmt->bindValue($key + 1, $value, $paramTypes[$key]);
    }

    // Execute the query
    $stmt->execute();

    // Fetch all the records
    $users = $stmt->fetchAll(PDO::FETCH_ASSOC);

    // Prepare the response data
    $response = [
        'page' => $page,
        'recordsPerPage' => $recordsPerPage,
        'data' => $users,
    ];

    // Output the data as JSON
    header('Content-Type: application/json');
    echo json_encode($response);
}

?>
